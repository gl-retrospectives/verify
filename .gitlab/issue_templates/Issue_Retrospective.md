<!--

Before submitting the issue, replace:

- $ISSUE with the issue link.

-->

This is an asynchronous retrospective for $ISSUE. It's private to the Verify
team, plus anyone else that worked with the team during $RELEASE. This
retrospective will be following the process described at
https://about.gitlab.com/handbook/engineering/management/team-retrospectives/

Please feel free to honestly describe your thoughts and feelings about working
on that release below.


/label ~retrospective
