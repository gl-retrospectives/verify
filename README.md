## Verify: Async Retros have moved as of %13.1

The asynchronous retrospectives of the Verify groups: **CI** and **Testing** can now be found under [`verify-stage`](https://gitlab.com/gl-retrospectives/verify-stage). (Note that Runner has their own subgroup)


